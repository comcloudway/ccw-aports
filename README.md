# 📦 APKBUILD Collection
 A collection of APKBUILDs that some may find useful


## Why not upstream your projects?
1. Most of my projects are not popular enough for me to publish them
2. If I were to publish them, I'd have to commit to maintaining the APKBUILD and the project. And I can't be bothered.

## Will you make prebuild binaries available?

Sorry 😔, but no.

## HELP! Something isn't working.
Before you `panic!`,
please check the according projects README,
just in case you've missed something.

But if you are certain that the problem is related to the `APKBUILD`,
[send me an email](mailto:comcloudway@ccw.icu).

## License
All the APKBUILDs in this repo are licensed under MIT unless noted otherwise.

Keep in mind,
that whilst you can use the APKBUILDs to your liking, 
the MIT might not apply to the projects themselves.

# Useful resources
The following list is inpired by the [pmaports repo](https://gitlab.com/postmarketOS/pmaports)
- [How to create a package](https://wiki.postmarketos.org/wiki/Create_a_package)
- [APKBUILD ref](https://wiki.alpinelinux.org/wiki/APKBUILD_Reference)
- [Creating an alpine package](https://wiki.alpinelinux.org/wiki/Creating_an_Alpine_package)
- [Alpine Linux aports](https://gitlab.alpinelinux.org/alpine/aports/)
- [Alpine Linux package search](https://pkgs.alpinelinux.org/packages)
